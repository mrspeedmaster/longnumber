//
//  longnumber.c
//  longnum_spa_1.1
//
//  Created by Bruno Balic on 01/12/2018.
//  Copyright © 2018 Bruno Balic. All rights reserved.
//

#include "longnumber.h"

// Digit - lista sa jednom znamenkom, LongNumber - Digit*
// "dugi" broj je lista znamenaka. Najmanje znacajna znamenka je na pocetku liste

// ne gradi novu listu nego preuredjuje dobivenu listu
LongNumber reverse(LongNumber num){
    LongNumber o = NULL;
    while (num != NULL) {
        LongNumber p = num;
        num = num->next;
        p->next = o;
        o = p;
    }
    return o;
}

LongNumber read_longnum(char* fname){
    // punim u glavu, vraca reversanu listu
    FILE *fd;
    fd = fopen(fname, "rt");
    if(fd == NULL){
        printf("Error opening file.\n");
        return NULL;
    }
    LongNumber prev = NULL;
    char tmp;
    while ((tmp = fgetc(fd)) != EOF) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = tmp - 48;
        newDig->next = prev;
        prev = newDig;
    }
    fclose(fd);
    return prev;
}

void write_longnum(LongNumber num, char* fname){
    num = reverse(num);
    LongNumber nptr = num;
    FILE *fd;
    fd = fopen(fname, "wt");
    while (num != NULL){
        fputc(num->z, fd);
        num = num->next;
    }
    fclose(fd);
    num = reverse(nptr);
}

void print_longnum(LongNumber num){
    num = reverse(num);
    LongNumber nptr = num;
    while (num != NULL){
        printf("%d", num->z);
        num = num->next;
    }
    printf("\n");
    num = reverse(nptr);
}

void delete_longnum(LongNumber num){
    LongNumber tmp;
    while (num != NULL){
        tmp = num;
        num = num->next;
        free(tmp);
    }
}

LongNumber add_longnum(LongNumber a, LongNumber b){
    LongNumber x, prev;
    prev = malloc(sizeof(Digit));
    prev->next = NULL;
    x = prev;
    int pretek = 0, anum, bnum;
    while ((a != NULL) || (b != NULL)) {
        if (a == NULL) {
            anum = 0;
        }
        else {
            anum = a->z;
            a = a->next;
        }
        if (b == NULL) {
            bnum = 0;
        }
        else {
            bnum = b->z;
            b = b->next;
        }
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = ((anum + bnum)%10 + pretek) % 10;
        pretek = (anum + bnum) / 10 + ((anum + bnum)%10 + pretek) / 10;
        newDig->next = NULL;
        prev->next = newDig;
        prev = newDig;
    }
    if (pretek) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = pretek;
        newDig->next = NULL;
        prev->next = newDig;
    }
    // izbrisi glavu
    LongNumber tmp;
    tmp = x;
    x = x->next;
    free(tmp);
    return x;
}

LongNumber mul_by_digit(LongNumber num, int z){
    LongNumber x, prev;
    prev = malloc(sizeof(Digit));
    prev->next = NULL;
    x = prev;
    int pretek = 0;
    while (num != NULL) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = (num->z * z) % 10 + pretek;
        pretek = (num->z * z) / 10;
        newDig->next = NULL;
        prev->next = newDig;
        prev = newDig;
        num = num->next;
    }
    if (pretek) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = pretek;
        newDig->next = NULL;
        prev->next = newDig;
    }
    LongNumber tmp;
    tmp = x;
    x = x->next;
    free(tmp);
    return x;
}

LongNumber mul_by_pow10(LongNumber num, int pow){
    LongNumber x, prev;
    prev = malloc(sizeof(Digit));
    prev->next = NULL;
    x = prev;
    for (int i = 0; i < pow; i++) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = 0;
        newDig->next = NULL;
        prev->next = newDig;
        prev = newDig;
    }
    while (num != NULL) {
        LongNumber newDig = malloc(sizeof(Digit));
        newDig->z = num->z;
        newDig->next = NULL;
        prev->next = newDig;
        prev = newDig;
        num = num->next;
    }
    LongNumber tmp;
    tmp = x;
    x = x->next;
    free(tmp);
    return x;
}

LongNumber mul_longnum(LongNumber a, LongNumber b){
    LongNumber x, res = NULL;
    x = res;
    int pow = 0;
    while (b != NULL) {
        if (b->z != 0) {
            LongNumber tmp1 = mul_by_digit(a, b->z);
            LongNumber tmp2 = mul_by_pow10(tmp1, pow);
            delete_longnum(tmp1);
            b = b->next;
            pow++;
            res = add_longnum(res, tmp2);
            delete_longnum(tmp2);
            delete_longnum(x);
            x = res;
        }
        else {
            b = b->next;
            pow++;
        }
    }
    return res;
}
