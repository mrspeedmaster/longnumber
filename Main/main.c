//
//  main.c
//  longnum_spa_1.1
//
//  Created by Bruno Balic on 01/12/2018.
//  Copyright © 2018 Bruno Balic. All rights reserved.
//

#include "longnumber.h"
#include <time.h>

int main(int argc, const char * argv[]) {
    
    clock_t start1, stop1;
    double res1;
    start1 = clock();
    
    LongNumber numa = read_longnum("/Users/B8/Documents/IT/3. semestar/SPA/dodatna_vjezba_longnum/a1.txt");
    printf("num a: ");
    print_longnum(numa);
    
    LongNumber numb = read_longnum("/Users/B8/Documents/IT/3. semestar/SPA/dodatna_vjezba_longnum/b1.txt");
    printf("num b: ");
    print_longnum(numb);
    
    LongNumber numc = add_longnum(numa, numb);
    printf("num c (a + b): ");
    print_longnum(numc);
    
    LongNumber numd = mul_by_digit(numc, 5);
    printf("num d (c * digit): ");
    print_longnum(numd);
    
    LongNumber nume = mul_by_pow10(numd, 3);
    printf("num e (d * pow): ");
    print_longnum(nume);
    
    LongNumber numf = mul_longnum(numa, numb);
    printf("num f (a * b): ");
    print_longnum(numf);
    
    stop1 = clock();
    res1 = ((double)(stop1 - start1)) / CLOCKS_PER_SEC;
    printf("%lf clock\n", res1);
    
    delete_longnum(numa);
    delete_longnum(numb);
    delete_longnum(numc);
    delete_longnum(numd);
    delete_longnum(nume);
    delete_longnum(numf);
    
    return 0;
}
